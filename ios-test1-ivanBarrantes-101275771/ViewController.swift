//
//  ViewController.swift
//  ios-test1-ivanBarrantes-101275771
//
//  Created by Ivan Barrantes on 2020-04-06.
//  Copyright © 2020 Ivan Barrantes. All rights reserved.
//


/*
 Please run it on an IPhone XR device
 */

import UIKit


class ViewController: UIViewController {
    
    @IBOutlet weak var milkNumber: UITextView!
    @IBOutlet weak var sugarNumber: UITextView!
    @IBOutlet weak var milkValue: UIStepper!
    @IBOutlet weak var sugarValue: UIStepper!
    @IBOutlet weak var typeValue: UISegmentedControl!
    @IBOutlet weak var sizeValue: UISegmentedControl!
    @IBOutlet weak var summaryText: UITextView!
    @IBOutlet weak var rewards: UISwitch!
    
    var price:Double = 0
    var finalPrice:Double = 0
    var typeOfCoffee:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func milkStepper(_ sender: UIStepper) {
        let milkDouble:Double = milkValue.value
        milkNumber.text = "\(milkDouble)"
    }
    
    @IBAction func sugarStepper(_ sender: UIStepper) {
        print("\(sugarValue.value)")
        let sugarDouble:Double = sugarValue.value
        sugarNumber.text = "\(sugarDouble)"
    }
    
    @IBAction func typeController(_ sender: UISegmentedControl) {
        print("Type Controller")
        switch typeValue.selectedSegmentIndex {
        case 0:
            milkValue.value = 0
            milkNumber.text = "0.0"
            sugarValue.value = 0
            sugarNumber.text = "0.0"
        case 1:
            milkValue.value = 1
            milkNumber.text = "1.0"
            sugarValue.value = 0.5
            sugarNumber.text = "0.5"
        case 2:
            milkValue.value = 2
            milkNumber.text = "2.0"
            sugarValue.value = 2
            sugarNumber.text = "2.0"
        default:
            break
        }
    }
    
    @IBAction func sizeController(_ sender: UISegmentedControl) {
        print("Size Controller")
        switch sizeValue.selectedSegmentIndex {
        case 0:
            price = 1.67
            finalPrice = price + (price * 0.13)
            typeOfCoffee = "Small"
        case 1:
            price = 1.89
            finalPrice = price + (price * 0.13)
            typeOfCoffee = "Medium"
        case 2:
            price = 2.10
            finalPrice = price + (round(price * 0.13))
            typeOfCoffee = "Large"
        default:
            break
        }
    }
    
    @IBAction func placeOrderButton(_ sender: Any) {
        if rewards.isOn {
            finalPrice = 0
        }
        summaryText.text = "Milk: \(milkValue.value)\tSugar: \(sugarValue.value)\nSize: \(typeOfCoffee)\nSubtotal: \(price)\nTax: \(rounded2(n: price * 0.13))\nRewards Applied: \(rewards.isOn)\nTotal: $\(rounded2(n: finalPrice))"
    }
    
    
    @IBAction func cancelOrderButton(_ sender: Any) {
        milkValue.value = 0
        milkNumber.text = "0.0"
        sugarValue.value = 0
        sugarNumber.text = "0.0"
        typeValue.selectedSegmentIndex = 0
        sizeValue.selectedSegmentIndex = 0
        rewards.isOn = false
        summaryText.text = ""
    }
    
    func rounded2 (n: Double) -> Double {
        let rounded = round(n * 100) / 100
        return rounded
    }
}

